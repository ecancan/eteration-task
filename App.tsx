import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import { Provider } from 'react-redux';
import MainNavigator from './src/navigators/MainNavigator';
import TabNavigator from './src/navigators/TabNavigator';
import LoadingProvider from './src/providers/LoadingProvider';
import ResultProvider from './src/providers/ResultProvider';
import { store } from './src/redux/store';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerShown: false }}>
          <Stack.Screen name={'TabNavigator'} component={TabNavigator}/>
          <Stack.Screen name={'MainNavigator'} component={MainNavigator}/>
        </Stack.Navigator>
      </NavigationContainer>
      <LoadingProvider/>
      <ResultProvider/>
      <StatusBar style="auto"/>
    </Provider>
  );
};

export default App;
