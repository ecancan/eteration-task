import React, { useEffect } from 'react';
import ActionButton from '../../src/components/common/button/ActionButton';
import useCartDispatcher from '../../src/hooks/useCartDispatcher';
import { useCart } from '../../src/hooks/useSlices';
import { CartItem } from '../../src/types/cartItemModel';
import { fireEvent, renderApp, screen } from '../../src/util/testUtil';

describe('useCartDispatcher', () => {
  const cartItemStateCallback = jest.fn((value) => value);

  beforeEach(() => {
    const Component = () => {
      const { clearAllItems } = useCartDispatcher();

      useEffect(() => {
        clearAllItems();
      }, []);

      return <></>;
    };

    renderApp({ children: <Component/> });
  });

  const product: CartItem = {
    createdAt: '2023-07-16T13:51:28.920Z',
    name: 'Dodge Mercielago',
    image: 'https://loremflickr.com/640/480/fashion',
    price: '873.00',
    description: 'description',
    model: 'Ranchero',
    brand: 'Smart',
    quantity: 1,
    total: 873,
    id: '11'
  };

  it('should be render correctly and get snapshot', () => {
    const Component = () => {
      useCartDispatcher();

      return <></>;
    };

    const container = renderApp({ children: <Component/> });

    expect(container).toMatchSnapshot();
  });

  it('should add cart item', () => {
    const Component = () => {
      const { items } = useCart();
      const { addItem } = useCartDispatcher();

      useEffect(() => {
        cartItemStateCallback(items);
      }, [items]);


      return <>
        <ActionButton label={'Add Item'} onPress={() => addItem({ product })}/>
      </>;
    };

    renderApp({ children: <Component/> });

    fireEvent.press(screen.getByText('Add Item'));

    expect(cartItemStateCallback).toBeCalledTimes(2);
    expect(cartItemStateCallback).toHaveReturnedWith([product]);
  });

  it('should add and remove cart item', () => {
    const Component = () => {
      const { items } = useCart();
      const { addItem, removeItem } = useCartDispatcher();

      useEffect(() => {
        cartItemStateCallback(items);
      }, [items]);

      return <>
        <ActionButton label={'Add Item'} onPress={() => addItem({ product })}/>
        <ActionButton label={'Remove Item'} onPress={() => removeItem({ id: product.id })}/>
      </>;
    };

    renderApp({ children: <Component/> });

    fireEvent.press(screen.getByText('Add Item'));
    fireEvent.press(screen.getByText('Remove Item'));

    expect(cartItemStateCallback).toHaveReturnedWith([]);
  });
});
