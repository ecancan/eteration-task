import { screen } from '@testing-library/react-native';
import React from 'react';
import CartCard from '../../../../src/components/cards/cart/CartCard';
import { CartItem } from '../../../../src/types/cartItemModel';
import { renderApp } from '../../../../src/util/testUtil';

describe('<CartCard />', () => {
  const callback = jest.fn();
  const product: CartItem = {
    createdAt: '2023-07-16T13:51:28.920Z',
    name: 'Dodge Mercielago',
    image: 'https://loremflickr.com/640/480/fashion',
    price: '873.00',
    description: 'description',
    model: 'Ranchero',
    brand: 'Smart',
    quantity: 1,
    total: 873,
    id: '11'
  };

  it('should be render correctly and get snapshot', () => {
    const Component = () => <CartCard item={product}/>;

    const container = renderApp({ children: <Component/> });

    expect(container).toMatchSnapshot();
  });

  it('should has item informations', () => {
    const Component = () => <CartCard item={product}/>;

    renderApp({ children: <Component/> });

    expect(screen.queryByText(product.name)).toBeTruthy();
    expect(screen.queryByText(product.price)).toBeTruthy();
    expect(screen.queryByText(String(product.quantity))).toBeTruthy();
    expect(screen.queryByText('+')).toBeTruthy();
    expect(screen.queryByText('-')).toBeTruthy();
  });
});
