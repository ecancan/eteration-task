import { fireEvent, screen } from '@testing-library/react-native';
import React from 'react';
import BaseButton from '../../../src/components/common/base-button/BaseButton';
import { renderApp } from '../../../src/util/testUtil';

describe('<BaseButton />', () => {
  const callback = jest.fn();

  it('should be render correctly and get snapshot', () => {
    const Component = () => <BaseButton label={'label'} onPress={callback}/>;

    const container = renderApp({ children: <Component/> });

    expect(container).toMatchSnapshot();
  });

  it('should has label', () => {
    const Component = () => <BaseButton label={'label'} onPress={callback}/>;

    renderApp({ children: <Component/> });

    expect(screen.queryByText('label')).toBeTruthy();
  });

  it('should trigger callback', () => {
    const Component = () => <BaseButton label={'label'} onPress={callback}/>;

    renderApp({ children: <Component/> });

    fireEvent.press(screen.getByText('label'));

    expect(callback).toBeCalled();
    expect(callback).toBeCalledTimes(1);
  });
});
