import React from 'react';

import { View } from 'react-native';
import CartCard from '../../../components/cards/cart/CartCard';
import BaseFlatList from '../../../components/common/base-flatlist/BaseFlatList';
import { useMain } from '../../../hooks/useSlices';
import { CartItem } from '../../../types/cartItemModel';

const FavoriteList = () => {
  const { favorites } = useMain();

  return (
    <View className={'relative'}>
      <View className={'py-2'}>
        <BaseFlatList data={favorites} renderItem={({ item }) => <CartCard item={item as CartItem} />} />
      </View>
    </View>
  );
};

export default FavoriteList;
