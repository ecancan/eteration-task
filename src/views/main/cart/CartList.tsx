import React from 'react';

import { View } from 'react-native';
import BottomActionCard from '../../../components/cards/bottom-action/BottomActionCard';
import CartCard from '../../../components/cards/cart/CartCard';
import BaseFlatList from '../../../components/common/base-flatlist/BaseFlatList';
import BaseLabel from '../../../components/common/base-label/BaseLabel';
import useCartDispatcher from '../../../hooks/useCartDispatcher';
import { useCart } from '../../../hooks/useSlices';
import { CartItem } from '../../../types/cartItemModel';
import { translate } from '../../../util/translateUtil';

const CartList = () => {
  const { items } = useCart();
  const { clearAllItems } = useCartDispatcher();

  const cartTotal = () =>
    items.reduce((prev, current) => parseFloat(String(prev)) + parseFloat(String(current.total)), 0);

  return (
    <View className={'relative'}>
      {items.length === 0 && (
        <View className={'w-full justify-center items-center h-full'}>
          <BaseLabel text={translate({ value: 'LABELS.NO_ITEM' })} />
        </View>
      )}
      <View className={'py-2'}>
        <BaseFlatList
          data={items}
          contentContainerStyle={{ paddingBottom: 80 }}
          renderItem={({ item }) => <CartCard item={item as CartItem} />}
        />
      </View>
      {items.length > 0 && (
        <BottomActionCard
          title={'Total'}
          subtitle={String(cartTotal())}
          button={{ label: 'Complete', onPress: clearAllItems }}
        />
      )}
    </View>
  );
};

export default CartList;
