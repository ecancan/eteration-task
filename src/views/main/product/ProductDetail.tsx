import { Image } from 'expo-image';
import React from 'react';

import { ScrollView, View } from 'react-native';
import BottomActionCard from '../../../components/cards/bottom-action/BottomActionCard';
import BaseLabel from '../../../components/common/base-label/BaseLabel';
import { ResultType } from '../../../enum/common.enum';
import useCartDispatcher from '../../../hooks/useCartDispatcher';
import useResultDispatcher from '../../../hooks/useResultDispatcher';
import { Product } from '../../../types/productModel';
import { translate } from '../../../util/translateUtil';

// @ts-ignore
const ProductDetail = ({ route, navigation }) => {
  const { product } = route.params;
  const { addItem } = useCartDispatcher();
  const { createResult } = useResultDispatcher();

  navigation.setOptions({ title: product?.name });

  const handleAddItem = ({ product }: { product: Product }) => {
    addItem({ product });

    createResult({ title: 'Success', message: 'Item added!', type: ResultType.SUCCESS });
  };

  return (
    <View className={'h-full flex-1'}>
      <ScrollView>
        <View className={'p-4 pb-32'}>
          <Image className={'w-full aspect-square mb-4 rounded-lg'} source={product.image} />
          <BaseLabel className={'font-bold text-xl mb-2'} text={product.name} />
          <BaseLabel text={product.description} />
        </View>
      </ScrollView>
      <BottomActionCard
        title={'Price'}
        containerClassName={'h-[120px]'}
        subtitle={String(product.price)}
        button={{ label: translate({ value: 'BUTTONS.ADD_TO_CART' }), onPress: () => handleAddItem({ product }) }}
      />
    </View>
  );
};

export default ProductDetail;
