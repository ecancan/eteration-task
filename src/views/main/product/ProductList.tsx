import React from 'react';

import { Pressable, TextInput, View } from 'react-native';
import ProductCard from '../../../components/cards/product/ProductCard';
import BaseFlatList from '../../../components/common/base-flatlist/BaseFlatList';
import useSearch from '../../../hooks/useSearch';
import { Product } from '../../../types/productModel';

// @ts-ignore
const ProductList = ({ navigation }) => {
  const { items: products, search } = useSearch();

  const renderItem = ({ item }: { item: unknown }) => (
    <Pressable
      onPress={() =>
        navigation.navigate('MainNavigator', {
          screen: 'ProductDetail',
          params: { product: item },
        })
      }
      className={'w-1/2 px-2'}
    >
      <ProductCard product={item as Product} />
    </Pressable>
  );

  return (
    <View className={'flex h-full'}>
      {products && (
        <>
          <View className={'w-full pt-2 px-2'}>
            <TextInput
              className={'h-[40px] bg-white px-4 border border-slate-400 rounded-md'}
              onChangeText={(value) => search({ value })}
              placeholder={'Search'}
            />
          </View>
          <View className={'flex flex-row h-full py-4'}>
            <BaseFlatList
              contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap', paddingBottom: 50 }}
              data={products}
              renderItem={renderItem}
              numColumns={2}
              onEndReachedThreshold={0.5}
            />
          </View>
        </>
      )}
    </View>
  );
};

export default ProductList;
