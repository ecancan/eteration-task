import { createSlice } from '@reduxjs/toolkit';

type MainSlice = {
  user: Record<string, unknown> | undefined;
  favorites: Array<Record<string, unknown>> | undefined;
};

const initialState: MainSlice = {
  user: undefined,
  favorites: undefined,
};

export const mainSlice = createSlice({
  name: 'main',
  initialState,
  reducers: {
    setUser(state, action) {
      state.user = action.payload;
    },
    setFavorites(state, action) {
      state.favorites = action.payload;
    },
  },
});

export const { setUser } = mainSlice.actions;
