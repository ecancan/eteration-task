import { createSlice } from '@reduxjs/toolkit';
import { CartItem } from '../../types/cartItemModel';

type CartSlice = {
  items: Array<CartItem>;
};

const initialState: CartSlice = {
  items: [],
};

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    setItems(state, action) {
      state.items = action.payload;
    },
  },
});

export const { setItems } = cartSlice.actions;
