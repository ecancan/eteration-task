import { createApi } from '@reduxjs/toolkit/query/react';
import { API_URL } from '../../../env';
import { ProvideTag } from '../../enum/apiServiceTag.enum';
import { ApiRequestProvider } from '../../providers/ApiRequestProvider';

const apiRequestProvider = new ApiRequestProvider({
  baseURL: API_URL,
  storageAuthKey: '-',
  storageCompanyKey: '-',
});

export const baseApi = createApi({
  reducerPath: 'api',
  baseQuery: apiRequestProvider.requester(),
  endpoints: () => ({}),
  tagTypes: Object.keys(ProvideTag),
});
