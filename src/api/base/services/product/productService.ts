import { ApiServiceMethod } from '../../../../enum/apiServiceMethods.enum';
import { Product } from '../../../../types/productModel';
import { Get } from '../../../commonService.interface';
import { ENDPOINT } from '../../../endpoints';
import { baseApi } from '../../baseApi';
import { ProductsQueryRequest } from './productService.interface';

export const productServiceApi = baseApi.injectEndpoints({
  endpoints: (builder) => ({
    getProducts: builder.query<Product[], Get<ProductsQueryRequest>>({
      query: ({ query }) => ({
        url: `${ENDPOINT.PRODUCTS}`,
        method: ApiServiceMethod.GET,
        data: { params: query },
      }),
    }),
  }),
});

export const { useGetProductsQuery } = productServiceApi;
