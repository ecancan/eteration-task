import { getLocales } from 'expo-localization';
import { I18n } from 'i18n-js';
import { LANGUAGES } from '../constant/common.constant';
import en from './en/en.json';

const i18n = new I18n({
  en: { ...en },
  enableFallback: true,
  locale: getLocales()[0].languageCode,
  defaultLocale: LANGUAGES.EN,
});

export default i18n;
