import { setItems } from '../redux/slices/cartSlice';
import { Product } from '../types/productModel';
import { useAppDispatch } from './useRedux';
import useResultDispatcher from './useResultDispatcher';
import { useCart } from './useSlices';

const useCartDispatcher = () => {
  const dispatch = useAppDispatch();
  const { items } = useCart();
  const { createResult } = useResultDispatcher();

  const addItem = ({ product }: { product: Product }) => {
    const getItem = items.find((item) => item.id === product.id);

    if (getItem) {
      const _items = items.map((item) => {
        if (item.id === product.id) {
          return {
            ...item,
            quantity: item.quantity + 1,
            total: (item?.quantity || 1) * parseFloat(item.price),
          };
        }

        return item;
      });

      dispatch(setItems(_items));

      return;
    }

    dispatch(setItems([...items, { ...product, quantity: 1, total: parseFloat(product.price) }]));
  };

  const removeItem = ({ id }: { id: string }) => {
    const getItem = items.find((item) => item.id === id);

    if (getItem?.quantity === 1) {
      dispatch(setItems(items.filter((item) => item.id !== id)));
    }

    if (getItem && getItem?.quantity > 1) {
      dispatch(
        setItems(
          items.map((item) => {
            if (item.id === id) {
              return {
                ...item,
                quantity: item.quantity - 1,
                total: (item?.quantity || 1) * parseFloat(item.price),
              };
            }

            return item;
          }),
        ),
      );
    }
  };

  const clearAllItems = () => dispatch(setItems([]));

  return {
    addItem,
    removeItem,
    clearAllItems,
  };
};

export default useCartDispatcher;
