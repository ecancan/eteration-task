import { useAppSelector } from './useRedux';

export const useMain = () => useAppSelector((state) => state.main);
export const useGlobalLoading = () => useAppSelector((state) => state.loading);
export const useResult = () => useAppSelector((state) => state.result);
export const useCart = () => useAppSelector((state) => state.cart);
