import { useEffect } from 'react';
import { Alert } from 'react-native';
import useResultDispatcher from './useResultDispatcher';
import { useResult } from './useSlices';

const useResultProvider = () => {
  const { results } = useResult();
  const { removeSpecificResult } = useResultDispatcher();

  useEffect(() => {
    results?.forEach((result) => {
      Alert.alert(result.title, result.message, [
        {
          text: 'OK',
          onPress: () => removeSpecificResult({ id: result.id }),
        },
      ]);
    });
  }, [results]);
};

export default useResultProvider;
