import { useEffect, useState } from 'react';
import { useGetProductsQuery } from '../api/base/services/product/productService';
import { Product } from '../types/productModel';

const useSearch = () => {
  const { data: products } = useGetProductsQuery({});
  const [items, setItems] = useState<Array<Product> | undefined>([]);

  const search = ({ value }: { value: string }) =>
    setItems(products?.filter((product) => product.name.toLocaleLowerCase().includes(value.toLocaleLowerCase())));

  useEffect(() => {
    (products || [])?.length > 0 && setItems(products);
  }, [products]);

  return {
    items,
    search,
  };
};

export default useSearch;
