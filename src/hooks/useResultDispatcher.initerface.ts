import { ResultType } from '../enum/common.enum';

export type ResultItem = {
  type: ResultType;
  title?: string;
  message: string;
};
