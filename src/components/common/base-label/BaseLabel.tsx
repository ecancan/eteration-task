import React, { FC } from 'react';

import { Text } from 'react-native';
import { twMerge } from 'tailwind-merge';
import { BaseLabelProps } from './BaseLabel.interface';

const BaseLabel: FC<BaseLabelProps> = (props) => {
  const { text, className } = props;

  const classes = twMerge(`
    text-sm
    ${className || ''}
  `);

  return (
    <Text {...props} className={classes}>
      {text}
    </Text>
  );
};

export default BaseLabel;
