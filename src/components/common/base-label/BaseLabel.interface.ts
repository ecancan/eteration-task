import { TextProps } from 'react-native';

export interface BaseLabelProps extends TextProps {
  text: string;
}
