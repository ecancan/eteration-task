import React, { FC } from 'react';
import { twMerge } from 'tailwind-merge';
import BaseButton from '../base-button/BaseButton';
import { ActionButtonProps } from './ActionButton.interface';

const ActionButton: FC<ActionButtonProps> = (props) => {
  const { containerClassName, labelClassName } = props;

  const classes = twMerge(`
    px-0 py-2 
    rounded-sm
    grow
    items-center
    justify-center
    bg-blue-700
    active:bg-blue-900
    ${containerClassName || ''}
  `);

  const labelClasses = twMerge(`
    text-white
    text-sm
    ${labelClassName || ''}
  `);

  return <BaseButton {...props} containerClassName={classes} labelClassName={labelClasses} />;
};

export default ActionButton;
