import { BaseButtonProps } from '../base-button/BaseButton.interface';

export interface ActionButtonProps extends BaseButtonProps {
  containerClassName?: string;
}
