import React, { FC } from 'react';

import { FlatList, FlatListProps } from 'react-native';

const BaseFlatList: FC<FlatListProps<unknown>> = (props) => (
  <FlatList bounces={false} initialNumToRender={12} {...props} />
);

export default BaseFlatList;
