import React, { FC } from 'react';

import { Text } from 'react-native';
import { twMerge } from 'tailwind-merge';
import { ErrorLabelProps } from './ErrorLabel.interface';

const ErrorLabel: FC<ErrorLabelProps> = (props) => {
  const { text, className } = props;

  const classes = twMerge(`
    text-red-700
    mt-4
    ${className}
  `);

  return (
    <Text {...props} className={classes}>
      {text}
    </Text>
  );
};

export default ErrorLabel;
