import { BaseLabelProps } from '../base-label/BaseLabel.interface';

export interface ErrorLabelProps extends BaseLabelProps {}
