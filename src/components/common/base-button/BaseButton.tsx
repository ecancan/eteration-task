import React, { FC } from 'react';

import { Pressable } from 'react-native';
import { twMerge } from 'tailwind-merge';
import BaseLabel from '../base-label/BaseLabel';
import { BaseButtonProps } from './BaseButton.interface';

const BaseButton: FC<BaseButtonProps> = (props) => {
  const { label, labelClassName, containerClassName } = props;

  const labelClasses = twMerge(`
    ${labelClassName || ''}
  `);

  return (
    <Pressable {...props} className={containerClassName}>
      <BaseLabel text={label} className={labelClasses} />
    </Pressable>
  );
};

export default BaseButton;
