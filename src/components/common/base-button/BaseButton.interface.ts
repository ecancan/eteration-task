import { GestureResponderEvent, PressableProps } from 'react-native';

export interface BaseButtonProps extends Omit<PressableProps, 'onPress'> {
  label: string;
  labelClassName?: string;
  onPress: null | ((event: GestureResponderEvent) => void) | undefined;
  containerClassName?: string;
}
