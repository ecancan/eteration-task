export interface BottomActionCardProps {
  title: string;
  subtitle?: string;
  containerClassName?: string;
  button?: {
    label: string;
    onPress: () => void;
  };
}
