import React, { FC } from 'react';

import { View } from 'react-native';
import { twMerge } from 'tailwind-merge';
import BaseLabel from '../../common/base-label/BaseLabel';
import ActionButton from '../../common/button/ActionButton';
import { BottomActionCardProps } from './BottomActionCard.interface';

const BottomActionCard: FC<BottomActionCardProps> = (props) => {
  const { title, subtitle, button, containerClassName } = props;

  const containerClasses = twMerge(`
    absolute z-50 bottom-0 left-0 w-full h-[90px] p-2
    ${containerClassName || ''}
  `);

  return (
    <View className={containerClasses}>
      <View className={'flex-row rounded-md border border-blue-300 items-center bg-white w-full justify-between p-4'}>
        <View className={'flex-1'}>
          <BaseLabel text={title} />
          {subtitle && <BaseLabel className={'text-md text-blue-700 font-bold'} text={subtitle} />}
        </View>
        {button && (
          <View className={'flex-1'}>
            <ActionButton label={button?.label} onPress={button?.onPress} />
          </View>
        )}
      </View>
    </View>
  );
};

export default BottomActionCard;
