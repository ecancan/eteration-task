import { Image } from 'expo-image';
import React, { FC } from 'react';

import { View } from 'react-native';
import useCartDispatcher from '../../../hooks/useCartDispatcher';
import { translate } from '../../../util/translateUtil';
import BaseLabel from '../../common/base-label/BaseLabel';
import ActionButton from '../../common/button/ActionButton';
import { ProductCardProps } from './ProductCard.interface';

const ProductCard: FC<ProductCardProps> = (props) => {
  const { product } = props;

  const { addItem } = useCartDispatcher();

  return (
    <View className={'w-full border border-slate-400 p-4 rounded-md mb-4 bg-white'}>
      <Image className={'w-full aspect-square rounded-md mb-4'} source={product.image} />
      <BaseLabel className={'font-medium text-blue-700'} text={product.price} />
      <View className={'h-[75px]'}>
        <BaseLabel className={'font-semibold text-lg mb-4'} text={product.name} />
      </View>
      <ActionButton
        className={'px-0 py-2 rounded-sm'}
        label={translate({ value: 'BUTTONS.ADD_TO_CART' })}
        onPress={() => addItem({ product })}
      />
    </View>
  );
};

export default ProductCard;
