import { Product } from '../../../types/productModel';

export interface ProductCardProps {
  product: Product;
}
