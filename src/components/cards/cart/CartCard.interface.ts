import { CartItem } from '../../../types/cartItemModel';

export interface CartCardProps {
  item: CartItem;
}
