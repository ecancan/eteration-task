import React, { FC } from 'react';

import { View } from 'react-native';
import useCartDispatcher from '../../../hooks/useCartDispatcher';
import BaseButton from '../../common/base-button/BaseButton';
import BaseLabel from '../../common/base-label/BaseLabel';
import { CartCardProps } from './CartCard.interface';

const CartCard: FC<CartCardProps> = (props) => {
  const { item } = props;
  const { addItem, removeItem } = useCartDispatcher();

  return (
    <View className={'m-2 p-4 rounded-lg bg-white border border-slate-300 flex-row justify-between'}>
      <View>
        <BaseLabel className={'text-sm font-bold'} text={item.name} />
        <BaseLabel className={'text-blue-700'} text={item.price} />
      </View>
      <View className={'flex-row'}>
        <BaseButton
          label={'-'}
          className={'px-4 py-1 bg-blue-200 items-center justify-center'}
          onPress={() => removeItem({ id: item.id })}
        />
        <BaseLabel className={'px-4 py-3 bg-slate-200 items-center justify-center'} text={String(item.quantity)} />
        <BaseButton
          label={'+'}
          className={'px-4 py-1 bg-blue-200 items-center justify-center'}
          onPress={() => addItem({ product: item })}
        />
      </View>
    </View>
  );
};

export default CartCard;
