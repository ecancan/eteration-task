import React, { FC } from 'react';

import { View } from 'react-native';
import BaseLabel from '../../common/base-label/BaseLabel';
import { FavoriteCardProps } from './FavoriteCard.interface';

const FavoriteCard: FC<FavoriteCardProps> = (props) => {
  const { item } = props;

  return (
    <View className={'m-2 p-4 rounded-lg bg-white border border-slate-300 flex-row justify-between'}>
      <View>
        <BaseLabel className={'text-sm font-bold'} text={item.name} />
        <BaseLabel className={'text-blue-700'} text={item.price} />
      </View>
    </View>
  );
};

export default FavoriteCard;
