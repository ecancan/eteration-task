import { CartItem } from '../../../types/cartItemModel';

export interface FavoriteCardProps {
  item: CartItem;
}
