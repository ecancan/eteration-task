export interface CartItem {
  createdAt: string;
  name: string;
  image: string;
  price: string;
  description: string;
  model: string;
  brand: string;
  id: string;
  quantity: number;
  total: number;
}
