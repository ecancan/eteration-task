import { render, RenderResult, act, fireEvent, screen, waitFor } from '@testing-library/react-native';
import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from '../redux/store';

const renderApp = ({ children }: { children: React.ReactElement }): RenderResult =>
  render(
    <Provider store={store}>
      <PersistGate persistor={persistor}>{children}</PersistGate>
    </Provider>,
  );

export { act, renderApp, waitFor, fireEvent, screen };
