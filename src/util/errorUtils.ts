export const getHttpCodeErrorTitle = (code: number | string) => `API_ERROR.GENERAL.TITLE (${code || '0'})`;
