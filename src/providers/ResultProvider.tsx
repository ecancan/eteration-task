import React from 'react';
import useResultProvider from '../hooks/useResultProvider';

const ResultProvider = () => {
  useResultProvider();

  return null;
};

export default ResultProvider;
