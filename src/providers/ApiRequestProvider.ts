import { Action, ThunkDispatch } from '@reduxjs/toolkit';
import { BaseQueryFn } from '@reduxjs/toolkit/dist/query/react';
import { Method } from 'axios';
import lodash from 'lodash';
import { silentServices } from '../api/silentServices';
import { ResultType } from '../enum/common.enum';
import { ApiErrorUseCase } from '../enum/error.enum';
import { decrease, increase } from '../redux/slices/loadingSlice';
import { addResult } from '../redux/slices/resultSlice';
import { BaseQueryFunctionParams } from './ApiRequestProvider.interface';
import { AxiosProvider } from './AxiosProvider';
import { ApiError } from './AxiosProvider.interface';

export class ApiRequestProvider {
  api;
  prefix;

  constructor({
    baseURL,
    prefix,
    headers,
    storageAuthKey,
    storageCompanyKey,
    additionalSuffixParams,
  }: {
    baseURL: string;
    prefix?: string;
    headers?: Record<string, string | number | boolean>;
    storageAuthKey: string;
    storageCompanyKey: string;
    additionalSuffixParams?: Record<string, unknown>;
  }) {
    this.api = new AxiosProvider({
      baseURL,
      headers,
      storageAuthKey,
      storageCompanyKey,
      additionalSuffixParams,
    });
    this.prefix = prefix;
  }

  private pushError = (error: ApiError, dispatch: ThunkDispatch<unknown, unknown, Action<unknown>>) => {
    dispatch(
      addResult({
        id: lodash.uniqueId(),
        type: ResultType.DANGER,
        title: error.errorTitle,
        message: error.errorLabel,
      }),
    );
  };

  private exceptionHandler = (error: ApiError, dispatch: ThunkDispatch<unknown, unknown, Action<unknown>>) => {
    if (error) {
      switch (error.useCase) {
        case ApiErrorUseCase.SHOW_MESSAGE:
          this.pushError(error, dispatch);
          break;
        default:
          break;
      }
    }
  };

  public requester
    = (): BaseQueryFn<BaseQueryFunctionParams> =>
    async ({ url, method, headers, data = {} }, { dispatch, endpoint }, extraOptions) => {
      const isSilentService = silentServices.includes(endpoint);

      try {
        !isSilentService && dispatch(increase());

        const result = await this.api.start({
          method: method as Method,
          url,
          prefix: this.prefix,
          body: data?.body,
          params: data?.params,
          headers,
          rest: {
            extraOptions,
          },
        });

        return { data: result?.data || result };
      } catch (error) {
        const apiError = error as ApiError;

        this.exceptionHandler(apiError, dispatch);

        return {
          error: apiError,
        };
      } finally {
        !isSilentService && dispatch(decrease());
      }
    };
}
