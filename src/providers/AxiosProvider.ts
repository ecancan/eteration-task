import AsyncStorage from '@react-native-async-storage/async-storage';
import axios, { AxiosInstance, Method } from 'axios';
import lodash from 'lodash';
import { getHttpCodeErrorTitle } from '../util/errorUtils';
import { ApiError } from './AxiosProvider.interface';
import { ApiErrorUseCase } from '../enum/error.enum';

export class AxiosProvider {
  // Variables
  axiosInstance: AxiosInstance;
  baseURL: string | undefined;
  headers: Record<string, string | number | boolean> | undefined;
  additionalSuffixParams: Record<string, unknown> | undefined;
  storageAuthKey: string;
  storageCompanyKey: string;

  constructor({
    baseURL,
    headers,
    storageAuthKey,
    storageCompanyKey,
    additionalSuffixParams,
  }: {
    baseURL: string;
    headers?: Record<string, string | number | boolean>;
    storageAuthKey: string;
    storageCompanyKey: string;
    additionalSuffixParams?: Record<string, unknown>;
  }) {
    this.axiosInstance = axios.create();
    this.storageAuthKey = storageAuthKey;
    this.storageCompanyKey = storageCompanyKey;
    this.baseURL = baseURL;
    this.setHeaders(headers);
    this.setAccessToken();
    this.additionalSuffixParams = additionalSuffixParams;
    this.setResponseInterceptors();
  }

  private setHeaders(_headers: Record<string, string | number | boolean> | undefined) {
    this.headers = _headers || {
      Accept: 'application/json',
    };
  }

  public async setAccessToken() {
    const raw = await AsyncStorage.getItem(this.storageAuthKey);

    const auth = (raw?.length && JSON.parse(raw)) || null;

    auth && (this.axiosInstance.defaults.headers.common.Authorization = `Bearer ${auth.access_token}`);
  }

  private setAdditionalSuffixParams() {
    return this.additionalSuffixParams;
  }

  private setResponseInterceptors() {
    this.axiosInstance.interceptors.response.use(
      (response) => response?.data || response,
      async (error) => {
        const errorResponse = error?.response;
        const errorStatusCode = errorResponse?.status;
        const errorMessage = errorResponse?.data?.description;

        const apiError: ApiError = {
          errorLabel: errorMessage,
          errorTitle: getHttpCodeErrorTitle(0),
          useCase: ApiErrorUseCase.SHOW_MESSAGE,
          statusCode: errorStatusCode,
        };

        return Promise.reject(apiError);
      },
    );
  }

  public async start({
    method,
    url,
    prefix,
    headers,
    body,
    params,
    rest,
  }: {
    method: Method;
    url: string;
    prefix?: string;
    headers?: Record<string, unknown> | unknown;
    body?: Record<string, unknown> | string | undefined | unknown;
    params?: Record<string, unknown> | string | undefined | unknown;
    rest?: Record<string, Record<string, unknown>>;
  }) {
    await this.setAccessToken();
    const queryParams = typeof params === 'object' ? params : {};

    const _params = rest?.extraOptions?.withoutAdditionalQueryParams
      ? queryParams
      : { ...this.setAdditionalSuffixParams(), ...queryParams };

    const axiosResponse = await this.axiosInstance({
      method,
      url,
      headers: { ...this.headers, ...(headers || {}) },
      baseURL: `${this.baseURL}${prefix || ''}`,
      data: body,
      params: _params,
      ...lodash.omit(rest, 'extraOptions'),
    });

    return axiosResponse;
  }
}
