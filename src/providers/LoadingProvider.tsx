import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import { useGlobalLoading } from '../hooks/useSlices';

const LoadingProvider = () => {
  const { loading } = useGlobalLoading();

  return loading > 0 ? (
    <View
      className={`
        w-full h-[100vh] 
        justify-center items-center 
        bg-black/20 dark:bg-white/20 
        backdrop-blur
        fixed top-0 left-0 z-50
      `}
    >
      <ActivityIndicator size='large' color='#000' />
    </View>
  ) : null;
};

export default LoadingProvider;
