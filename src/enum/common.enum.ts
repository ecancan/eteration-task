export enum ResultType {
  SUCCESS = 'success',
  WARNING = 'warning',
  DANGER = 'danger',
}
