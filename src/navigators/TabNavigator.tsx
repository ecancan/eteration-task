import { Ionicons } from '@expo/vector-icons';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { View } from 'react-native';
import BaseLabel from '../components/common/base-label/BaseLabel';
import { useCart } from '../hooks/useSlices';
import { translate } from '../util/translateUtil';
import CartList from '../views/main/cart/CartList';
import FavoriteList from '../views/main/favorite/FavoriteList';
import ProductList from '../views/main/product/ProductList';
import ProfileDetail from '../views/main/profile/ProfileDetail';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const { items } = useCart();

  const cartItemCount = () => items?.reduce((prev, current) => Number(prev) + Number(current.quantity), 0);

  return (
    <Tab.Navigator>
      <Tab.Screen
        name={translate({ value: 'VIEWS.PRODUCTS.TITLE' })}
        component={ProductList}
        options={{
          tabBarIcon: ({ color, size }) => <Ionicons name={'ios-home-outline'} size={size} color={color} />,
        }}
      />
      <Tab.Screen
        name={translate({ value: 'VIEWS.CART.TITLE' })}
        component={CartList}
        options={{
          tabBarIcon: ({ color, size }) => (
            <View className={'relative'}>
              {items?.length > 0 && (
                <View
                  className={
                    'absolute top-0 -right-2 bg-red-500 px-1 min-w-[15px] h-[15px] z-50 rounded-full items-center justify-center'
                  }
                >
                  <View>
                    <BaseLabel className={'text-white text-xs'} text={String(cartItemCount())} />
                  </View>
                </View>
              )}
              <Ionicons name={'ios-cart-outline'} size={size} color={color} />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name={translate({ value: 'VIEWS.FAVORITE.TITLE' })}
        component={FavoriteList}
        options={{
          tabBarIcon: ({ color, size }) => <Ionicons name={'ios-star-outline'} size={size} color={color} />,
        }}
      />
      <Tab.Screen
        name={translate({ value: 'VIEWS.PROFILE.TITLE' })}
        component={ProfileDetail}
        options={{
          tabBarIcon: ({ color, size }) => <Ionicons name={'ios-person-outline'} size={size} color={color} />,
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
