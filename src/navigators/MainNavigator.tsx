import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import ProductDetail from '../views/main/product/ProductDetail';

type MainNavigatorStackList = {
  ProductDetail: undefined;
};

const Stack = createStackNavigator<MainNavigatorStackList>();

const MainNavigator = () => (
  <Stack.Navigator screenOptions={{ headerBackTitleVisible: false }}>
    <Stack.Screen name='ProductDetail' component={ProductDetail} />
  </Stack.Navigator>
);

export default MainNavigator;
