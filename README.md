# Note

I unfortunately couldn't dedicate much time to the given task. However, I believe I managed to complete many things within the time I allocated. Due to not being able to allocate more time, a few gaps remain. These gaps are, in order: the infinity load, favorites, and filtering modal. Additionally, I haven't completed all of them with unit tests, but to demonstrate, I've written one of each from different components and hooks. The total time I've spent on the entire task is 7 hours and 34 minutes. Since I won't be able to do everything perfectly, I wanted to focus on the parts where I can showcase my understanding of specific structures. My only request from you is to provide constructive feedback or support that can help me improve, considering the equivalent of 7 hours. Thank you in advance.

# Folder and File Naming Standards

- Naming case type of util files: `$camelCaseUtil.ts`
- Naming case type of enum files: `$camelCase.enum.ts`
- Naming case type of constant files: `$camelCase.constant.ts`
- Naming case type of folder: `$kebab-case`
- Naming case type of components: `$PascalCase.tsx`
- Naming case type of interface file: `$PascalCase.interface.ts`
- Naming case type of test file: `$PascalCase.test.ts|tsx`

# Installation Steps
## For macOSx
### Install Brew

If you not have brew package manager you have to this tool.
Below are the commands required to install

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Detail information: https://brew.sh/index_tr

### Install NodeJS with NVM (version: 18.xx)

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
```
This project supported node versions are `node>=16.19.0 node<=18.17.1`

If it didn't add path. You have to add by manually
```
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
```

Let's install nodejs with nvm
```
    nvm install 18.17.1
```
Detail information: https://github.com/nvm-sh/nvm

### Project Installation and Development start
```
yarn install
yarn start or expo start -c
```

#### How to run unit tests?
```
yarn run test
```

You can check `package.json` file for other scripts
